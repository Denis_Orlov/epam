package by.epamcourses.task_hospital.database.DAO;

import by.epamcourses.task_hospital.database.MYSQL.MySQLDAOFactory;

public abstract class DAOFactory {
    /**
     * The list of supported databases.
     */
    public enum Factories {
        MYSQL
    }

    public abstract DiagnosisDAO getDiagnosisDAO();

    public abstract DrugsDAO getDrugsDAO();

    public abstract DischargedDAO getDischargedDAO();

    public abstract OperationsDAO getOperationsDAO();

    public abstract PatientDAO getPatientDAO();

    public abstract PatientsDiagnosisDAO getPatientDiagnosisDAO();

    public abstract PatientsDrugsDAO getPatientDrugsDAO();

    public abstract PatientsOperationsDAO getPatientOperationsDAO();

    public abstract PatientsProceduresDAO getPatientProceduresDAO();

    public abstract ProceduresDAO getProceduresDAO();

    public abstract UserDAO getUserDAO();

    public static DAOFactory getDAOFactory(Factories factoryName) {
        switch (factoryName) {
            case MYSQL:
                return new MySQLDAOFactory();
            default:
                return null;
        }
    }

}
