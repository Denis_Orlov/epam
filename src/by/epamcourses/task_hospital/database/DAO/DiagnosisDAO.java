package by.epamcourses.task_hospital.database.DAO;

import by.epamcourses.task_hospital.entity.Diagnosis;

import java.util.LinkedList;

public interface DiagnosisDAO {
    LinkedList<Diagnosis> findAll();

    Diagnosis find(Integer diagnosisID);

    Diagnosis findDiagnosisOfPatient(Integer patientID);

    boolean createNewDiagnosis(String diagnosisName);

    Integer findIdOfDiagnosis(String diagnosisName);

    boolean deleteDiagnosis(Integer diagnosisID);
}
