package by.epamcourses.task_hospital.database.DAO;

import by.epamcourses.task_hospital.entity.Diagnosis;
import by.epamcourses.task_hospital.entity.Patient;

import java.util.LinkedList;

public interface DischargedDAO {

    boolean dischargePatient(Patient patient, Diagnosis diagnosis);

    LinkedList<String> getAllDischarged();

    LinkedList<String> getAllDischargedBySurname(String surname);

    LinkedList<String> getAllDischargedByFullName(String name, String surname, String patronymic);
}
