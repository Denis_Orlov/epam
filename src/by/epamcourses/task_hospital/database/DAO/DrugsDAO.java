package by.epamcourses.task_hospital.database.DAO;

import by.epamcourses.task_hospital.entity.Drugs;

import java.util.LinkedList;

public interface DrugsDAO {
    LinkedList<Drugs> findAll();

    Drugs find(Integer drugsID);

    LinkedList<Drugs> findDrugsOfPatient(Integer patientID);

    boolean createNewDrugs(String drugsName);

    Integer findIdOfDrugs(String drugsName);

    boolean deleteDrugs(Integer drugsID);
}
