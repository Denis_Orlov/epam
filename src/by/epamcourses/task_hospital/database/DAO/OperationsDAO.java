package by.epamcourses.task_hospital.database.DAO;

import by.epamcourses.task_hospital.entity.Operations;

import java.util.LinkedList;

public interface OperationsDAO {

    LinkedList<Operations> findAll();

    Operations find(Integer operationsID);

    LinkedList<Operations> findOperationsOfPatient(Integer patientID);

    boolean createNewOperations(String operationsName);

    Integer findIdOfOperations(String operationsName);

    boolean deleteOperation(Integer operationsID);
}
