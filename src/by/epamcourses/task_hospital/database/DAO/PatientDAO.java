package by.epamcourses.task_hospital.database.DAO;

import by.epamcourses.task_hospital.entity.Patient;

import java.util.LinkedList;

public interface PatientDAO {

    LinkedList<Patient> findAll();

    Patient find(Integer patientID);

    LinkedList<Patient> findPatientsBySurname(String surname);

    Integer findIdOfPatient(Patient patient);

    boolean addNewPatient(Patient patient);

    boolean updatePatient(Patient patient);

    boolean deletePatient(Patient patient);

    boolean deletePatientByID(Integer ID);
}
