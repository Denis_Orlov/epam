package by.epamcourses.task_hospital.database.DAO;

import by.epamcourses.task_hospital.entity.Diagnosis;
import by.epamcourses.task_hospital.entity.Patient;

public interface PatientsDiagnosisDAO {

    boolean createPatientsDiagnosis(Patient patient, Diagnosis diagnosis);

    Diagnosis getPatientDiagnosis(Patient patient);

    boolean deletePatientDiagnosis(Patient patient, Diagnosis diagnosis);


    public boolean changeDiagnosis(Patient patient, Diagnosis oldDiagnosis,
                                   Diagnosis newDiagnosis);
}
