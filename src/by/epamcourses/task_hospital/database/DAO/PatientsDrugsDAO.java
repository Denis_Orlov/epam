package by.epamcourses.task_hospital.database.DAO;

import by.epamcourses.task_hospital.entity.Drugs;
import by.epamcourses.task_hospital.entity.Patient;

import java.util.LinkedList;

public interface PatientsDrugsDAO {

    boolean createPatientsDrugs(Patient patient, Drugs drugs);

    LinkedList<Drugs> getPatientDrugs(Patient patient);

    boolean deletePatientDrug(Patient patient, Drugs drugs);
}
