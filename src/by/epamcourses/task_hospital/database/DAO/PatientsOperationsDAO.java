package by.epamcourses.task_hospital.database.DAO;

import by.epamcourses.task_hospital.entity.Operations;
import by.epamcourses.task_hospital.entity.Patient;

import java.util.LinkedList;

public interface PatientsOperationsDAO {

    boolean createPatientsOperations(Patient patient, Operations operation);

    LinkedList<Operations> getPatientOperations(Patient patient);

    boolean deletePatientOperation(Patient patient, Operations operation);
}
