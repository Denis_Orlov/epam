package by.epamcourses.task_hospital.database.DAO;

import by.epamcourses.task_hospital.entity.Patient;
import by.epamcourses.task_hospital.entity.Procedures;

import java.util.LinkedList;

public interface PatientsProceduresDAO {

    boolean createPatientsProcedures(Patient patient, Procedures procedures);

    LinkedList<Procedures> getPatientsProcedures(Patient patient);

    boolean deletePatientsProcedures(Patient patient, Procedures procedures);
}
