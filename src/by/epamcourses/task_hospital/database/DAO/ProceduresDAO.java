package by.epamcourses.task_hospital.database.DAO;

import by.epamcourses.task_hospital.entity.Procedures;

import java.util.LinkedList;

public interface ProceduresDAO {

    LinkedList<Procedures> findAll();

    Procedures find(Integer proceduresID);

    LinkedList<Procedures> findProceduresOfPatient(Integer patientID);

    boolean createNewProcedures(String proceduresName);

    Integer findIdOfProcedures(String proceduresName);

    boolean deleteProcedures(Integer proceduresID);
}
