package by.epamcourses.task_hospital.database.DAO;

import by.epamcourses.task_hospital.entity.User;

import java.util.LinkedList;

public interface UserDAO {

    LinkedList<User> findAll();

    User find(Integer userID);

    User find(String userLogin);

    boolean createNewUser(User newUser);
    public boolean isExist(String login);
    Integer findIdOfUser(String userName);

    boolean verifyUser(String login, String password);
}
