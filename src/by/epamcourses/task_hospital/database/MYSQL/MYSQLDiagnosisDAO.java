package by.epamcourses.task_hospital.database.MYSQL;

import by.epamcourses.task_hospital.database.DAO.DiagnosisDAO;
import by.epamcourses.task_hospital.database.utils.ConnectionPool;
import by.epamcourses.task_hospital.database.utils.JDBCUtil;
import by.epamcourses.task_hospital.entity.Diagnosis;
import org.apache.log4j.Logger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

public class MYSQLDiagnosisDAO implements DiagnosisDAO {

    private static final Logger log = Logger.getLogger(MYSQLDiagnosisDAO.class);

    public MYSQLDiagnosisDAO() {
    }

    @Override
    public LinkedList<Diagnosis> findAll() {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet res = null;
        LinkedList<Diagnosis> diagnosis = null;
        try {
            /**
             * Get Connection and create PreparedStatement.
             */
            conn = ConnectionPool.getConnection();
            stat = conn.prepareStatement("SELECT * FROM diagnosis");
            res = stat.executeQuery();
            diagnosis = new LinkedList<Diagnosis>();
            res.beforeFirst();
            while (res.next()) {
                Diagnosis temp = new Diagnosis();
                temp.setDiagnosisID(res.getInt(1));
                temp.setDiagnosisName(res.getString(2));
                diagnosis.add(temp);
            }

        } catch (SQLException e) {
            log.error(e);
        } finally {
            JDBCUtil.close(conn);
            JDBCUtil.close(stat);
            JDBCUtil.close(res);
        }
        return diagnosis;
    }

    @Override
    public Diagnosis find(Integer diagnosisID) {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet res = null;
        Diagnosis diagnosis = null;
        try {
            /**
             * Get Connection and create PreparedStatement.
             */
            conn = ConnectionPool.getConnection();
            stat = conn.prepareStatement("SELECT * FROM diagnosis WHERE id=?;");
            stat.setInt(1, diagnosisID);
            res = stat.executeQuery();
            res.beforeFirst();
            while (res.next()) {
                diagnosis = new Diagnosis();
                diagnosis.setDiagnosisID(res.getInt(1));
                diagnosis.setDiagnosisName(res.getString(2));
            }
        } catch (SQLException e) {
            log.error(e);
        } finally {
            JDBCUtil.close(conn);
            JDBCUtil.close(stat);
            JDBCUtil.close(res);
        }
        return diagnosis;
    }

    @Override
    public Diagnosis findDiagnosisOfPatient(Integer patientID) {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet res = null;
        Diagnosis diagnosis = null;

        try {
            /**
             * Get Connection and create PreparedStatement.
             */
            conn = ConnectionPool.getConnection();
            stat = conn.prepareStatement("SELECT * FROM diagnosis WHERE diagnosis.ID in(SELECT diagnosisID FROM patientdiagnosis WHERE patientID=?);");
            stat.setInt(1, patientID);
            res = stat.executeQuery();
            res.beforeFirst();
            /**
             *Retrieve information from the result set.
             */
            if (res.next()) {
                diagnosis = new Diagnosis();
                diagnosis.setDiagnosisID(res.getInt(1));
                diagnosis.setDiagnosisName(res.getString(2));
            }
        } catch (SQLException e) {
            log.error(e);
        } finally {
            JDBCUtil.close(conn);
            JDBCUtil.close(stat);
            JDBCUtil.close(res);
        }
        return diagnosis;
    }

    @Override
    public boolean createNewDiagnosis(String diagnosisName) {
        Connection conn = null;
        PreparedStatement stat = null;
        try {
            /**
             * Get Connection and create PreparedStatement.
             */
            conn = ConnectionPool.getConnection();
            stat = conn.prepareStatement("INSERT INTO diagnosis (diaName) VALUES (?);");
            stat.setString(1, diagnosisName);
            return stat.execute();
        } catch (SQLException e) {
            log.error(e);
        } finally {
            JDBCUtil.close(conn);
            JDBCUtil.close(stat);
        }
        return false;
    }

    @Override
    public Integer findIdOfDiagnosis(String diagnosisName) {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet res = null;
        Integer result = null;

        try {
            /**
             * Get Connection and create PreparedStatement.
             */
            conn = ConnectionPool.getConnection();
            stat = conn.prepareStatement("SELECT id FROM diagnosis WHERE diaName=?;");
            stat.setString(1, diagnosisName);
            res = stat.executeQuery();
            res.beforeFirst();
            /**
             *Retrieve information from the result set.
             */
            if (res.next()) {
                result = res.getInt(1);
            }
        } catch (SQLException e) {
            log.error(e);
        } finally {
            JDBCUtil.close(conn);
            JDBCUtil.close(stat);
            JDBCUtil.close(res);
        }
        return result;
    }

    @Override
    public boolean deleteDiagnosis(Integer diagnosisID) {
        Connection conn = null;
        PreparedStatement stat = null;
        try {
            /**
             * Get Connection and create PreparedStatement.
             */
            conn = ConnectionPool.getConnection();
            stat = conn.prepareStatement("DELETE from diagnosis WHERE id=?;");
            stat.setInt(1, diagnosisID);
            return stat.execute();
        } catch (SQLException e) {
            log.error(e);
        } finally {
            JDBCUtil.close(conn);
            JDBCUtil.close(stat);
        }
        return false;
    }
}
