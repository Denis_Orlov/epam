package by.epamcourses.task_hospital.database.MYSQL;

import by.epamcourses.task_hospital.database.DAO.DischargedDAO;
import by.epamcourses.task_hospital.database.utils.ConnectionPool;
import by.epamcourses.task_hospital.database.utils.JDBCUtil;
import by.epamcourses.task_hospital.entity.Diagnosis;
import by.epamcourses.task_hospital.entity.Patient;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;


public class MYSQLDischargedDAO implements DischargedDAO {
    private static final Logger log = Logger.getLogger(MYSQLDiagnosisDAO.class);

    public MYSQLDischargedDAO() {
    }

    @Override
    public boolean dischargePatient(Patient patient, Diagnosis diagnosis) {

        Connection conn = null;
        PreparedStatement stat = null;
        try {
            conn = ConnectionPool.getConnection();
            stat = conn.prepareStatement("INSERT INTO discharged (patientID, name, surname, pathronimic, diagnosisName)" +
                    " VALUES (?,?,?,?,?);");
            stat.setInt(1, patient.getPatientID());
            stat.setString(2, patient.getPatientName());
            stat.setString(3, patient.getPatientSurname());
            stat.setString(4, patient.getPatientPatronymic());
            stat.setString(5, diagnosis.getDiagnosisName());
            return stat.execute();
        } catch (SQLException e) {
            log.error(e);
        } finally {
            JDBCUtil.close(conn);
            JDBCUtil.close(stat);
        }
        return false;
    }

    @Override
    public LinkedList<String> getAllDischarged() {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet res = null;
        LinkedList<String> discharged = null;

        try {
            conn = ConnectionPool.getConnection();
            stat = conn.prepareStatement("SELECT * FROM discharged;");
            res = stat.executeQuery();
            discharged = new LinkedList<String>();
            res.beforeFirst();
            /**
             *Retrieve information from the result set.
             */
            while (res.next()) {
                StringBuilder sb = new StringBuilder();
                sb.append(res.getInt(1)).append(". ");
                sb.append(res.getString(2)).append(" ");
                sb.append(res.getString(3)).append(" ");
                sb.append(res.getString(4)).append(" - ");
                sb.append(res.getString(5)).append(".");
                discharged.add(sb.toString());
            }
        } catch (SQLException e) {
            log.error(e);
        } finally {
            JDBCUtil.close(conn);
            JDBCUtil.close(stat);
            JDBCUtil.close(res);
        }
        return discharged;
    }

    @Override
    public LinkedList<String> getAllDischargedBySurname(String surname) {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet res = null;
        LinkedList<String> discharged = null;

        try {
            conn = ConnectionPool.getConnection();
            stat = conn.prepareStatement("SELECT * FROM discharged WHERE surname = ?;");
            stat.setString(1, surname);
            res = stat.executeQuery();
            discharged = new LinkedList<String>();
            res.beforeFirst();
            while (res.next()) {
                StringBuilder sb = new StringBuilder();
                sb.append(res.getInt(1)).append(". ");
                sb.append(res.getString(2)).append(" ");
                sb.append(res.getString(3)).append(" ");
                sb.append(res.getString(4)).append(" - ");
                sb.append(res.getString(5)).append(".");
                discharged.add(sb.toString());
            }
        } catch (SQLException e) {
            log.error(e);
        } finally {
            JDBCUtil.close(conn);
            JDBCUtil.close(stat);
            JDBCUtil.close(res);
        }
        return discharged;
    }

    @Override
    public LinkedList<String> getAllDischargedByFullName(String name,
                                                         String surname, String patronymic) {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet res = null;
        LinkedList<String> discharged = null;

        try {
            conn = ConnectionPool.getConnection();
            stat = conn.prepareStatement("SELECT * FROM discharged WHERE name=? and surname=? and patronymic=?;");
            stat.setString(1, name);
            stat.setString(2, surname);
            stat.setString(3, patronymic);
            res = stat.executeQuery();
            discharged = new LinkedList<String>();
            res.beforeFirst();
            while (res.next()) {
                StringBuilder sb = new StringBuilder();
                sb.append(res.getInt(1)).append(". ");
                sb.append(res.getString(2)).append(" ");
                sb.append(res.getString(3)).append(" ");
                sb.append(res.getString(4)).append(" - ");
                sb.append(res.getString(5)).append(".");
                discharged.add(sb.toString());
            }
        } catch (SQLException e) {
            log.error(e);
        } finally {
            JDBCUtil.close(conn);
            JDBCUtil.close(stat);
            JDBCUtil.close(res);
        }
        return discharged;
    }
}
