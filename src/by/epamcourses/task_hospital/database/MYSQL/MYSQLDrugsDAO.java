package by.epamcourses.task_hospital.database.MYSQL;


import by.epamcourses.task_hospital.database.DAO.DrugsDAO;
import by.epamcourses.task_hospital.database.utils.ConnectionPool;
import by.epamcourses.task_hospital.database.utils.JDBCUtil;
import by.epamcourses.task_hospital.entity.Drugs;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

public class MYSQLDrugsDAO implements DrugsDAO {
    private static final Logger log = Logger.getLogger(MYSQLDrugsDAO.class);

    public MYSQLDrugsDAO() {
    }

    @Override
    public LinkedList<Drugs> findAll() {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet res = null;
        LinkedList<Drugs> drugs = null;

        try {
            conn = ConnectionPool.getConnection();
            stat = conn.prepareStatement("SELECT * FROM drugs;");
            res = stat.executeQuery();
            drugs = new LinkedList<Drugs>();
            res.beforeFirst();
            while (res.next()) {
                Drugs temp = new Drugs();
                temp.setDrugsID(res.getInt(1));
                temp.setDrugsName(res.getString(2));
                drugs.add(temp);
            }
        } catch (SQLException e) {
            log.error(e);
        } finally {
            JDBCUtil.close(conn);
            JDBCUtil.close(stat);
            JDBCUtil.close(res);
        }
        return drugs;
    }

    @Override
    public Drugs find(Integer drugsID) {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet res = null;
        Drugs drugs = null;

        try {
            conn = ConnectionPool.getConnection();
            stat = conn.prepareStatement("SELECT * FROM drugs WHERE id=?;");
            stat.setInt(1, drugsID);
            res = stat.executeQuery();
            res.beforeFirst();
            if (res.next()) {
                drugs = new Drugs();
                drugs.setDrugsID(res.getInt(1));
                drugs.setDrugsName(res.getString(2));
            }
        } catch (SQLException e) {
            log.error(e);
        } finally {
            JDBCUtil.close(conn);
            JDBCUtil.close(stat);
            JDBCUtil.close(res);
        }
        return drugs;
    }

    @Override
    public LinkedList<Drugs> findDrugsOfPatient(Integer patientID) {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet res = null;
        LinkedList<Drugs> drugs = null;

        try {
            conn = ConnectionPool.getConnection();
            stat = conn.prepareStatement("SELECT * FROM drugs WHERE drugs.ID in(SELECT drugsID FROM patientdrugs WHERE patientID=?);");
            stat.setInt(1, patientID);
            res = stat.executeQuery();
            res.beforeFirst();
            drugs = new LinkedList<Drugs>();
            while (res.next()) {
                Drugs temp = new Drugs();
                temp.setDrugsID(res.getInt(1));
                temp.setDrugsName(res.getString(2));
                drugs.add(temp);
            }
        } catch (SQLException e) {
            log.error(e);
        } finally {
            JDBCUtil.close(conn);
            JDBCUtil.close(stat);
            JDBCUtil.close(res);
        }
        return drugs;
    }

    @Override
    public boolean createNewDrugs(String drugsName) {
        Connection conn = null;
        PreparedStatement stat = null;
        try {
            conn = ConnectionPool.getConnection();
            stat = conn.prepareStatement("INSERT INTO drugs (drugName) VALUES (?);");
            stat.setString(1, drugsName);
            return stat.execute();
        } catch (SQLException e) {
            log.error(e);
        } finally {
            JDBCUtil.close(conn);
            JDBCUtil.close(stat);
        }
        return false;
    }

    @Override
    public Integer findIdOfDrugs(String drugsName) {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet res = null;
        Integer result = null;

        try {
            conn = ConnectionPool.getConnection();
            stat = conn.prepareStatement("SELECT id FROM drugs WHERE drugName=?;");
            stat.setString(1, drugsName);
            res = stat.executeQuery();
            res.beforeFirst();
            if (res.next()) {
                result = res.getInt(1);
            }
        } catch (SQLException e) {
            log.error(e);
        } finally {
            JDBCUtil.close(conn);
            JDBCUtil.close(stat);
            JDBCUtil.close(res);
        }
        return result;
    }

    @Override
    public boolean deleteDrugs(Integer drugsID) {
        Connection conn = null;
        PreparedStatement stat = null;
        try {
            conn = ConnectionPool.getConnection();
            stat = conn.prepareStatement("DELETE from drugs WHERE id=?;");
            stat.setInt(1, drugsID);
            return stat.execute();
        } catch (SQLException e) {
            log.error(e);
        } finally {
            JDBCUtil.close(conn);
            JDBCUtil.close(stat);
        }
        return false;
    }
}
