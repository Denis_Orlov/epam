package by.epamcourses.task_hospital.database.MYSQL;

import by.epamcourses.task_hospital.database.DAO.PatientsDrugsDAO;
import by.epamcourses.task_hospital.database.utils.ConnectionPool;
import by.epamcourses.task_hospital.database.utils.JDBCUtil;
import by.epamcourses.task_hospital.entity.Drugs;
import by.epamcourses.task_hospital.entity.Patient;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

public class MYSQLPatientsDrugsDAO implements PatientsDrugsDAO {
    private static final Logger log = Logger.getLogger(MYSQLPatientsDrugsDAO.class);
    public MYSQLPatientsDrugsDAO() {
    }

    @Override
    public boolean createPatientsDrugs(Patient patient, Drugs drug) {
        Connection conn = null;
        PreparedStatement stat = null;
        try {
            conn = ConnectionPool.getConnection();
            stat = conn.prepareStatement("INSERT INTO patientdrugs (patientID, drugsID) VALUES (?, ?);");
            stat.setInt(1, patient.getPatientID());
            stat.setInt(2, drug.getDrugsID());
            return stat.execute();
        } catch (SQLException e) {
            log.error(e);
        }finally{
            JDBCUtil.close(conn);
            JDBCUtil.close(stat);
        }
        return false;
    }

    @Override
    public LinkedList<Drugs> getPatientDrugs(Patient patient) {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet res = null;
        LinkedList<Drugs> drugs = null;

        try {
            conn = ConnectionPool.getConnection();
            stat = conn.prepareStatement("SELECT * FROM drugs WHERE drugs.ID in(SELECT drugsID FROM patientdrugs WHERE patientID=?);");
            stat.setInt(1, patient.getPatientID());
            res = stat.executeQuery();
            res.beforeFirst();
            drugs = new LinkedList<Drugs>();
            while(res.next()){
                Drugs temp = new Drugs();
                temp.setDrugsID(res.getInt(1));
                temp.setDrugsName(res.getString(2));
                drugs.add(temp);
            }
        } catch (SQLException e) {
            log.error(e);
        }finally{
            JDBCUtil.close(conn);
            JDBCUtil.close(stat);
            JDBCUtil.close(res);
        }
        return drugs;
    }

    @Override
    public boolean deletePatientDrug(Patient patient, Drugs drug) {
        Connection conn = null;
        PreparedStatement stat = null;
        try {
            conn = ConnectionPool.getConnection();
            stat = conn.prepareStatement("DELETE FROM patientdrugs WHERE patientID=? and drugsID=?;");
            stat.setInt(1, patient.getPatientID());
            stat.setInt(2, drug.getDrugsID());
            return stat.execute();
        } catch (SQLException e) {
            log.error(e);
        }finally{
            JDBCUtil.close(conn);
            JDBCUtil.close(stat);
        }
        return false;
    }
}
