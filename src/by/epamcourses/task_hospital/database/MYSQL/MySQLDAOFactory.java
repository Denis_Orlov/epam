package by.epamcourses.task_hospital.database.MYSQL;

import by.epamcourses.task_hospital.database.DAO.*;
import by.epamcourses.task_hospital.database.MYSQL.*;

public class MySQLDAOFactory extends DAOFactory{
    @Override
    public DiagnosisDAO getDiagnosisDAO() {
        return new MYSQLDiagnosisDAO();
    }

    @Override
    public DrugsDAO getDrugsDAO() {
        return new MYSQLDrugsDAO();
    }

    @Override
    public DischargedDAO getDischargedDAO() {
        return new MYSQLDischargedDAO();
    }

    @Override
    public OperationsDAO getOperationsDAO() {
        return new MYSQLOperationsDAO();
    }

    @Override
    public PatientDAO getPatientDAO() {
        return new MYSQLPatientDAO();
    }

    @Override
    public PatientsDiagnosisDAO getPatientDiagnosisDAO() {
        return new MYSQLPatientsDiagnosisDAO();
    }

    @Override
    public PatientsDrugsDAO getPatientDrugsDAO() {
        return new MYSQLPatientsDrugsDAO();
    }

    @Override
    public PatientsOperationsDAO getPatientOperationsDAO() {
        return new MYSQLPatientsOperationsDAO();
    }

    @Override
    public PatientsProceduresDAO getPatientProceduresDAO() {
        return new MYSQLPatientsProceduresDAO();
    }

    @Override
    public ProceduresDAO getProceduresDAO() {
        return new MYSQLProceduresDAO();
    }

    @Override
    public UserDAO getUserDAO() {
        return new MYSQLUserDAO();
    }
}
