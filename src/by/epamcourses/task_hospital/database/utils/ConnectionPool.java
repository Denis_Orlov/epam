package by.epamcourses.task_hospital.database.utils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * ConnectionPool TomCat from javax.sql.DataSource
 */
public class ConnectionPool {

    private static DataSource dataSource;

    public static synchronized Connection getConnection() {
        if (dataSource == null) {
            try {
                /**
                 * Get initial context that has references to all configurations and
                 * resources defined for this web application.
                 */
                Context initContext = new InitialContext();
                /**
                 * Get Context object for all environment naming (JNDI), such as
                 * Resources configured for this web application.
                 */
                Context envContext = (Context) initContext.lookup("java:/comp/env");
                /**
                 * Name of the Resource we want to access.
                 */
                dataSource = (DataSource) envContext.lookup("jdbc/hospital");
            } catch (NamingException e) {
                System.out.println("Cannot find the data source");
            }
        }

        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            System.out.println("Cannot establish connection");
            return null;
        }
    }

}
