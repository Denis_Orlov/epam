package by.epamcourses.task_hospital.database.utils;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCUtil {
    private static final Logger log = Logger.getLogger(JDBCUtil.class);

    public static void close(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void close(ResultSet resultSet){
        try{
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void close(Statement statement){
        try{
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
