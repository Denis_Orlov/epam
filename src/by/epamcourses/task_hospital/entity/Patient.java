package by.epamcourses.task_hospital.entity;

public class Patient {

    private Integer patientID;
    private String patientName;
    private String patientSurname;
    private String patientPatronymic;

    public Patient() {
    }

    public Patient(String patientName, Integer patientID, String patientSurname,
                   String patientPatronymic) {
        this.patientName = patientName;
        this.patientID = patientID;
        this.patientSurname = patientSurname;
        this.patientPatronymic = patientPatronymic;
    }

    public Integer getPatientID() {
        return patientID;
    }

    public void setPatientID(Integer patientID) {
        this.patientID = patientID;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientSurname() {
        return patientSurname;
    }

    public void setPatientSurname(String patientSurname) {
        this.patientSurname = patientSurname;
    }

    public String getPatientPatronymic() {
        return patientPatronymic;
    }

    public void setPatientPatronymic(String patientPatronymic) {
        this.patientPatronymic = patientPatronymic;
    }
}
