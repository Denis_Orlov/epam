package by.epamcourses.task_hospital.web.commands;

import by.epamcourses.task_hospital.database.DAO.DiagnosisDAO;
import by.epamcourses.task_hospital.database.DAO.DrugsDAO;
import by.epamcourses.task_hospital.database.DAO.OperationsDAO;
import by.epamcourses.task_hospital.database.DAO.ProceduresDAO;
import by.epamcourses.task_hospital.entity.User;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;


public class AdminCommand extends Command {
    private static final Logger log = Logger.getLogger(UserLoginCommand.class);

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {

        String doAction = (String) request.getAttribute("doaction");

        if (request.getSession().getAttribute("user") == null ||
                ((User) request.getSession().getAttribute("user")).getRole() != User.ADMINISTRATOR || doAction == null)
            return "login";

        try {
            request.setCharacterEncoding("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        DiagnosisDAO diagnosisDao = null;
        DrugsDAO drugsDao = null;
        OperationsDAO operationsDao = null;
        ProceduresDAO proceduresDao = null;

        switch (doAction) {

            case "deletediagnosis":
                Integer diagnosisID = Integer.parseInt(request.getParameter("deletediagnosis"));
                diagnosisDao = daoFactory.getDiagnosisDAO();
                if (!diagnosisDao.deleteDiagnosis(diagnosisID))
                    log.info("Diagnosis " + diagnosisID + " deleted from database");
                break;
            case "adddiagnosis":
                String diagnosisName = request.getParameter("diagnosis");
                diagnosisDao = daoFactory.getDiagnosisDAO();
                if (!diagnosisDao.createNewDiagnosis(diagnosisName))
                    log.info("Diagnosis " + diagnosisName + " added to database");
                break;
            case "deletedrug":
                Integer drugsID = Integer.parseInt(request.getParameter("deletedrug"));
                drugsDao = daoFactory.getDrugsDAO();
                if (!drugsDao.deleteDrugs(drugsID))
                    log.info("Drug " + drugsID + " deleted from database");
                break;
            case "adddrug":
                String drugsName = request.getParameter("drug");
                drugsDao = daoFactory.getDrugsDAO();
                if (!drugsDao.createNewDrugs(drugsName))
                    log.info("Drug " + drugsName + " added to database");
                break;
            case "deleteprocedure":
                Integer proceduresID = Integer.parseInt(request.getParameter("deleteprocedure"));
                proceduresDao = daoFactory.getProceduresDAO();
                if (!proceduresDao.deleteProcedures(proceduresID))
                    log.info("Procedure " + proceduresID + " deleted from database");
                break;
            case "addprocedure":
                String proceduresName = request.getParameter("procedure");
                proceduresDao = daoFactory.getProceduresDAO();
                if (!proceduresDao.createNewProcedures(proceduresName))
                    log.info("Procedure " + proceduresName + " added to database");
                break;
            case "deleteoperation":
                Integer operationsID = Integer.parseInt(request.getParameter("deleteoperation"));
                operationsDao = daoFactory.getOperationsDAO();
                if (!operationsDao.deleteOperation(operationsID))
                    log.info("Operation " + operationsID + " deleted from database");
                break;
            case "addoperation":
                String operationsName = request.getParameter("operation");
                operationsDao = daoFactory.getOperationsDAO();
                if (!operationsDao.createNewOperations(operationsName))
                    log.info("Operation " + operationsName + " added to database");
                break;
        }
        return new AdministrateCommand().execute(request, response);
    }
}
