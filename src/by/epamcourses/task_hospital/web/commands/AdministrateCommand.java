package by.epamcourses.task_hospital.web.commands;

import by.epamcourses.task_hospital.database.DAO.DiagnosisDAO;
import by.epamcourses.task_hospital.database.DAO.DrugsDAO;
import by.epamcourses.task_hospital.database.DAO.OperationsDAO;
import by.epamcourses.task_hospital.database.DAO.ProceduresDAO;
import by.epamcourses.task_hospital.entity.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedList;


public class AdministrateCommand extends Command {
    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {
        if (request.getSession().getAttribute("user") == null ||
                ((User) request.getSession().getAttribute("user")).getRole() != User.ADMINISTRATOR)
            return "main";
        DiagnosisDAO diagnosisDAO = daoFactory.getDiagnosisDAO();
        DrugsDAO drugsDAO = daoFactory.getDrugsDAO();
        OperationsDAO operationsDAO = daoFactory.getOperationsDAO();
        ProceduresDAO proceduresDAO = daoFactory.getProceduresDAO();


        LinkedList<Diagnosis> allDiagnosis = diagnosisDAO.findAll();
        LinkedList<Drugs> allDrugs = drugsDAO.findAll();
        LinkedList<Procedures> allProcedures = proceduresDAO.findAll();
        LinkedList<Operations> allOperations = operationsDAO.findAll();

        request.setAttribute("alldiagnosis", allDiagnosis);
        request.setAttribute("alldrugs", allDrugs);
        request.setAttribute("allprocedures", allProcedures);
        request.setAttribute("alloperations", allOperations);

        return "admin";
    }
}
