package by.epamcourses.task_hospital.web.commands;


import by.epamcourses.task_hospital.database.DAO.DiagnosisDAO;
import by.epamcourses.task_hospital.database.DAO.PatientsDiagnosisDAO;
import by.epamcourses.task_hospital.entity.Diagnosis;
import by.epamcourses.task_hospital.entity.Patient;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ChangePatientDiagnosisCommand extends Command {

    private static final Logger log = Logger.getLogger(ChangePatientDiagnosisCommand.class);

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {
        Integer diagnosisID = Integer.parseInt(request.getParameter("changeddiagnosis"));
        Patient patient = (Patient) request.getSession().getAttribute("patient");
        DiagnosisDAO diagnosisDAO = daoFactory.getDiagnosisDAO();
        PatientsDiagnosisDAO patientsDiagnosisDAO = daoFactory.getPatientDiagnosisDAO();
        Diagnosis newDiagnosis = diagnosisDAO.find(diagnosisID);
        Diagnosis oldDiagnosis = patientsDiagnosisDAO.getPatientDiagnosis(patient);
        if (oldDiagnosis != null) {
            if (!patientsDiagnosisDAO.changeDiagnosis(patient, oldDiagnosis, newDiagnosis))
                log.info("Changed diagnosis of patient " + patient.getPatientID() + " to " + newDiagnosis.getDiagnosisName());
        } else {
            if (!patientsDiagnosisDAO.createPatientsDiagnosis(patient, newDiagnosis))
                log.info("Defined diagnosis to patient " + patient.getPatientID() + ": " + newDiagnosis.getDiagnosisName());
        }
        return new PatientCommand().execute(request, response);
    }


}
