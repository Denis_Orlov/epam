package by.epamcourses.task_hospital.web.commands;

import by.epamcourses.task_hospital.database.DAO.DAOFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class Command {
    public static DAOFactory daoFactory;

    public static void setDaoFactory(DAOFactory factory) {
        daoFactory = factory;
    }

    public abstract String execute(HttpServletRequest request, HttpServletResponse response);
}
