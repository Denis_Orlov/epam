package by.epamcourses.task_hospital.web.commands;


import by.epamcourses.task_hospital.database.DAO.DAOFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

public class CommandFactory {
    private HashMap<String, Command> commands;
    private static CommandFactory instance;

    public CommandFactory() {

        commands = new HashMap<String, Command>();
        /**
         *set DAOFactory for all commands
         */
        Command.setDaoFactory(DAOFactory.getDAOFactory(DAOFactory.Factories.MYSQL));
        /**
         * create commands and set mapping
         */
        commands.put("/registration", new UserRegistrationCommand());
        commands.put("/login", new UserLoginCommand());
        commands.put("/logout", new LogOutCommand());
        commands.put("/", new IndexCommand());
        commands.put("/viewallpatients", new ViewAllPatientsCommand());
        commands.put("/administrate", new AdministrateCommand());
        commands.put("/patient", new PatientCommand());
        commands.put("/performdrugs", new PerformDrugsCommand());
        commands.put("/performprocedures", new PerformProceduresCommand());
        commands.put("/performoperations", new PerformOperationsCommand());
        commands.put("/adddrug", new AddPatientDrugCommand());
        commands.put("/addoperation", new AddPatientOperationsCommand());
        commands.put("/addprocedure", new AddPatientProceduresCommand());
        commands.put("/changediagnosis", new ChangePatientDiagnosisCommand());
        commands.put("/submituser", new SubmitUserCommand());
        commands.put("/newpatient", new NewPatientCommand());
        commands.put("/submitpatient", new SubmitPatientCommand());
        commands.put("/admin", new AdminCommand());
        commands.put("/dischargepatient", new DischargeCommand());
        commands.put("/viewdischargedpatients", new ViewDischargedCommand());


    }

    public static synchronized CommandFactory getInstance() {
        if (instance == null) {
            instance = new CommandFactory();
        }
        return instance;
    }

    public Command getCommand(HttpServletRequest request) {
        String path = request.getRequestURI().substring(request.getContextPath().length());
        return commands.get(path);
    }
}
