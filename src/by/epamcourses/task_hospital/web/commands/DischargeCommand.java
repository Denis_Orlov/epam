package by.epamcourses.task_hospital.web.commands;

import by.epamcourses.task_hospital.database.DAO.DischargedDAO;
import by.epamcourses.task_hospital.database.DAO.PatientDAO;
import by.epamcourses.task_hospital.database.DAO.PatientsDiagnosisDAO;
import by.epamcourses.task_hospital.entity.Diagnosis;
import by.epamcourses.task_hospital.entity.Patient;
import by.epamcourses.task_hospital.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class DischargeCommand extends Command {
    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {
        User user = (User) request.getSession().getAttribute("user");
        if (user == null || user.getRole() != 2)
            return "login";
        PatientDAO patientDAO = daoFactory.getPatientDAO();
        PatientsDiagnosisDAO patientsDiagnosisDAO = daoFactory.getPatientDiagnosisDAO();
        DischargedDAO dischargedDAO = daoFactory.getDischargedDAO();

        Patient patient = (Patient) request.getSession().getAttribute("patient");
        Diagnosis diagnosis = patientsDiagnosisDAO.getPatientDiagnosis(patient);
        dischargedDAO.dischargePatient(patient, diagnosis);
        patientDAO.deletePatientByID(patient.getPatientID());
        request.getSession().setAttribute("patient", null);
        return "main";
    }
}
