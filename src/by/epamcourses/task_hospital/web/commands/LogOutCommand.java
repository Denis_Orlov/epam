package by.epamcourses.task_hospital.web.commands;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogOutCommand extends Command {
    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {
        request.getSession().setAttribute("user", null);
        request.getSession().setAttribute("notlogged", false);
        return "login";
    }
}
