package by.epamcourses.task_hospital.web.commands;

import by.epamcourses.task_hospital.database.DAO.DiagnosisDAO;
import by.epamcourses.task_hospital.entity.Diagnosis;
import by.epamcourses.task_hospital.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedList;

public class NewPatientCommand extends Command {
    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {
        User user = (User) request.getSession().getAttribute("user");
        if (user == null || user.getRole() != 2)
            return "login";
        DiagnosisDAO diagnosisDAO = daoFactory.getDiagnosisDAO();
        LinkedList<Diagnosis> allDiagnosis = diagnosisDAO.findAll();
        request.setAttribute("alldiagnosis", allDiagnosis);
        return "newpatient";
    }

}
