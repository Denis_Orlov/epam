package by.epamcourses.task_hospital.web.commands;

import by.epamcourses.task_hospital.database.DAO.*;
import by.epamcourses.task_hospital.entity.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedList;

public class PatientCommand extends Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        PatientDAO patientDao = daoFactory.getPatientDAO();
        PatientsDiagnosisDAO patientsDiagnosisDAO = daoFactory.getPatientDiagnosisDAO();
        PatientsDrugsDAO patientsDrugsDAO = daoFactory.getPatientDrugsDAO();
        PatientsOperationsDAO patientsOperationsDAO = daoFactory.getPatientOperationsDAO();
        PatientsProceduresDAO patientsProceduresDAO = daoFactory.getPatientProceduresDAO();
        DiagnosisDAO diagnosisDAO = daoFactory.getDiagnosisDAO();
        DrugsDAO drugsDAO = daoFactory.getDrugsDAO();
        OperationsDAO operationsDAO = daoFactory.getOperationsDAO();
        ProceduresDAO proceduresDAO = daoFactory.getProceduresDAO();

        Patient patient = patientDao.find((Integer) request.getSession().getAttribute("patientID"));
        Diagnosis diagnosis = patientsDiagnosisDAO.getPatientDiagnosis(patient);
        LinkedList<Drugs> drugs = patientsDrugsDAO.getPatientDrugs(patient);
        LinkedList<Procedures> procedures = patientsProceduresDAO.getPatientsProcedures(patient);
        LinkedList<Operations> operations = patientsOperationsDAO.getPatientOperations(patient);
        LinkedList<Diagnosis> allDiagnosis = diagnosisDAO.findAll();
        LinkedList<Drugs> allDrugs = drugsDAO.findAll();
        LinkedList<Procedures> allProcedures = proceduresDAO.findAll();
        LinkedList<Operations> allOperations = operationsDAO.findAll();

        request.getSession().setAttribute("patient", patient);
        request.setAttribute("diagnosis", diagnosis);
        request.setAttribute("drugs", drugs);
        request.setAttribute("procedures", procedures);
        request.setAttribute("operations", operations);
        request.setAttribute("alldiagnosis", allDiagnosis);
        request.setAttribute("alldrugs", allDrugs);
        request.setAttribute("allprocedures", allProcedures);
        request.setAttribute("alloperations", allOperations);
        return "patient";
    }
}
