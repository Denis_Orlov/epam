package by.epamcourses.task_hospital.web.commands;

import by.epamcourses.task_hospital.database.DAO.DrugsDAO;
import by.epamcourses.task_hospital.database.DAO.PatientsDrugsDAO;
import by.epamcourses.task_hospital.entity.Drugs;
import by.epamcourses.task_hospital.entity.Patient;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class PerformDrugsCommand extends Command {
    private static final Logger log = Logger.getLogger(PerformDrugsCommand.class);

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {

        Integer drugsID = Integer.parseInt(request.getParameter("performeddrug"));
        Patient patient = (Patient) request.getSession().getAttribute("patient");
        DrugsDAO drugsDAO = daoFactory.getDrugsDAO();
        PatientsDrugsDAO patientsDrugsDAO = daoFactory.getPatientDrugsDAO();
        Drugs drug = drugsDAO.find(drugsID);
        if (!patientsDrugsDAO.deletePatientDrug(patient, drug))
            log.info("Performed " + drug.getDrugsName() + " to patient " + patient.getPatientID());
        return new PatientCommand().execute(request, response);
    }
}
