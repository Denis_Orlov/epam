package by.epamcourses.task_hospital.web.commands;

import by.epamcourses.task_hospital.database.DAO.OperationsDAO;
import by.epamcourses.task_hospital.database.DAO.PatientsOperationsDAO;
import by.epamcourses.task_hospital.entity.Operations;
import by.epamcourses.task_hospital.entity.Patient;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PerformOperationsCommand extends Command {
    private static final Logger log = Logger.getLogger(PerformOperationsCommand.class);

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {
        Integer operationsID = Integer.parseInt(request.getParameter("performedoperations"));
        Patient patient = (Patient) request.getSession().getAttribute("patient");
        OperationsDAO operationsDAO = daoFactory.getOperationsDAO();
        PatientsOperationsDAO patientsOperationsDAO = daoFactory.getPatientOperationsDAO();
        Operations operation = operationsDAO.find(operationsID);
        if (!patientsOperationsDAO.deletePatientOperation(patient, operation))
            log.info("Performed " + operation.getOperationsName() + " to patient " + patient.getPatientID());
        return new PatientCommand().execute(request, response);
    }
}
