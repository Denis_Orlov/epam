package by.epamcourses.task_hospital.web.commands;

import by.epamcourses.task_hospital.database.DAO.PatientsProceduresDAO;
import by.epamcourses.task_hospital.database.DAO.ProceduresDAO;
import by.epamcourses.task_hospital.entity.Patient;
import by.epamcourses.task_hospital.entity.Procedures;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PerformProceduresCommand extends Command {
    private static final Logger log = Logger.getLogger(PerformProceduresCommand.class);

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {
        Integer proceduresID = Integer.parseInt(request.getParameter("performedprocedures"));
        Patient patient = (Patient) request.getSession().getAttribute("patient");
        ProceduresDAO proceduresDAO = daoFactory.getProceduresDAO();
        PatientsProceduresDAO patientsProceduresDAO = daoFactory.getPatientProceduresDAO();
        Procedures procedure = proceduresDAO.find(proceduresID);
        if (!patientsProceduresDAO.deletePatientsProcedures(patient, procedure))
            log.info("Performed " + procedure.getProceduresName() + " to patient " + patient.getPatientID());
        return new PatientCommand().execute(request, response);
    }
}
