package by.epamcourses.task_hospital.web.commands;


import by.epamcourses.task_hospital.database.DAO.DiagnosisDAO;
import by.epamcourses.task_hospital.database.DAO.PatientDAO;
import by.epamcourses.task_hospital.database.DAO.PatientsDiagnosisDAO;
import by.epamcourses.task_hospital.entity.Diagnosis;
import by.epamcourses.task_hospital.entity.Patient;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;

public class SubmitPatientCommand extends Command {
    private static final Logger log = Logger.getLogger(SubmitPatientCommand.class);

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {
        try {
            request.setCharacterEncoding("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String name = (String) request.getParameter("patientName");
        String surname = (String) request.getParameter("patientSurname");
        String patronymic = (String) request.getParameter("patientPatronymic");
        Integer diagnosisID = Integer.parseInt(request.getParameter("diagnosis"));
        if (name != null && surname != null && patronymic != null && diagnosisID != null) {
            PatientDAO patientDAO = daoFactory.getPatientDAO();
            PatientsDiagnosisDAO patientsDiagnosisDAO = daoFactory.getPatientDiagnosisDAO();
            DiagnosisDAO diagnosisDAO = daoFactory.getDiagnosisDAO();

            Patient patient = new Patient();
            patient.setPatientName(name);
            patient.setPatientSurname(surname);
            patient.setPatientPatronymic(patronymic);

            patientDAO.addNewPatient(patient);
            Integer patientID = patientDAO.findIdOfPatient(patient);
            patient = patientDAO.find(patientID);
            Diagnosis diagnosis = diagnosisDAO.find(diagnosisID);
            if (!patientsDiagnosisDAO.createPatientsDiagnosis(patient, diagnosis))
                log.info("added new patient. id = " + patient.getPatientID());
            return "main";
        }
        return "newpatient";
    }
}
