package by.epamcourses.task_hospital.web.commands;

import by.epamcourses.task_hospital.database.DAO.UserDAO;
import by.epamcourses.task_hospital.entity.User;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;


public class SubmitUserCommand extends Command {

    private static final Logger log = Logger.getLogger(SubmitUserCommand.class);

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {
        try {
            request.setCharacterEncoding("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        UserDAO userDAO = daoFactory.getUserDAO();
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String repassword = request.getParameter("repassword");
        Integer role = Integer.parseInt(request.getParameter("role"));
        if (login != null && password != null && password.equals(repassword) && role != null && !userDAO.isExist(login)) {
            User newUser = new User();
            newUser.setLogin(login);
            newUser.setPassword(password);
            newUser.setRole(role);
            if (userDAO.createNewUser(newUser)) {
                log.info("added new user. Login = " + newUser.getLogin());
                return "main";
            }
        }
        return "userregistration";
    }
}
