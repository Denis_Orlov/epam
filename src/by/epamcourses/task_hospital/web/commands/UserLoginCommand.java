package by.epamcourses.task_hospital.web.commands;


import by.epamcourses.task_hospital.database.DAO.UserDAO;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UserLoginCommand extends Command {
    private static final Logger log = Logger.getLogger(UserLoginCommand.class);

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {
        UserDAO userDAO = daoFactory.getUserDAO();
        String login = (String) request.getParameter("login");
        String password = (String) request.getParameter("password");
        if (userDAO.verifyUser(login, password)) {
            request.getSession().setAttribute("user", userDAO.find(login));
            log.info("User " + login + " logged in");
            return "main";
        }
        request.getSession().setAttribute("notlogged", true);
        return "login";
    }
}
