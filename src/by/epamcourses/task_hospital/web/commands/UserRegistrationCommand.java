package by.epamcourses.task_hospital.web.commands;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class UserRegistrationCommand extends Command {
    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {
        return "userregistration";
    }
}
