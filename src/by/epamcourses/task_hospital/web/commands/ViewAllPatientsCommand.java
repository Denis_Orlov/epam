package by.epamcourses.task_hospital.web.commands;

import by.epamcourses.task_hospital.database.DAO.PatientDAO;
import by.epamcourses.task_hospital.entity.Patient;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedList;

public class ViewAllPatientsCommand extends Command {
    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {
        PatientDAO patientDAO = daoFactory.getPatientDAO();
        LinkedList<Patient> patients = patientDAO.findAll();
        request.getSession().setAttribute("patients", patients);
        return "patients";
    }

}
