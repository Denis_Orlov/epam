package by.epamcourses.task_hospital.web.commands;

import by.epamcourses.task_hospital.database.DAO.DischargedDAO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedList;

public class ViewDischargedCommand extends Command {

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {
        DischargedDAO dischargedDAO = daoFactory.getDischargedDAO();
        LinkedList<String> discharged = dischargedDAO.getAllDischarged();
        request.getSession().setAttribute("discharged", discharged);

        return "discharged";
    }
}
