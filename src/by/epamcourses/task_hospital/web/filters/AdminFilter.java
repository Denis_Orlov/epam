package by.epamcourses.task_hospital.web.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter(dispatcherTypes = {
        DispatcherType.REQUEST,
        DispatcherType.FORWARD
}
        , urlPatterns = { "/admin/*" })
public class AdminFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        String requestURI = req.getRequestURI();
        String pattern = "/admin/";
        if(requestURI.startsWith(pattern)){
            String doAction = requestURI.substring(pattern.length());
            req.setAttribute("doaction", doAction);
            req.getRequestDispatcher("/admin").forward(request, response);
        }else
            chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
