package by.epamcourses.task_hospital.web.filters;

import by.epamcourses.task_hospital.web.utils.LanguageBundle;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Servlet Filter implementation class LanguageFilter
 */
@WebFilter(dispatcherTypes = {
        DispatcherType.REQUEST,
        DispatcherType.FORWARD
}
        , urlPatterns = { "/language/*" })
public class LanguageFilter implements Filter {
    public LanguageFilter() { }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        String requestURI = req.getRequestURI();
        String pattern = "/language/";
        if(requestURI.startsWith(pattern)){
            String lang = requestURI.substring(pattern.length());
            LanguageBundle.addLanguage(lang + ".properties", req);
            req.getRequestDispatcher("/").forward(request, response);
        }
        else
            chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
