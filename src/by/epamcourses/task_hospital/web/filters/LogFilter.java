package by.epamcourses.task_hospital.web.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter(dispatcherTypes = {
        DispatcherType.REQUEST
}
        , servletNames = { "FrontController" })
public class LogFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        String path = req.getRequestURI().substring(req.getContextPath().length());
        if(req.getSession().getAttribute("user") == null && !path.equals("/login") && !path.equals("/registration")
                && !path.equals("/submituser")){
            req.getRequestDispatcher("/logout").forward(request, response);
        }
        else
            chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
