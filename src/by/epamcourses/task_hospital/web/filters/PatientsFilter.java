package by.epamcourses.task_hospital.web.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


public class PatientsFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        String requestURI = req.getRequestURI();
        String pattern = "/patient/";
        if (requestURI.startsWith(pattern)) {
            Integer patientID = Integer.parseInt(requestURI.substring(pattern.length()));
            req.getSession().setAttribute("patientID", patientID);
            req.getRequestDispatcher("/patient").forward(request, response);
        } else
            chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
