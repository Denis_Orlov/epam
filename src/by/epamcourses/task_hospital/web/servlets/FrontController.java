package by.epamcourses.task_hospital.web.servlets;

import by.epamcourses.task_hospital.web.commands.Command;
import by.epamcourses.task_hospital.web.commands.CommandFactory;
import by.epamcourses.task_hospital.web.utils.LanguageBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FrontController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public FrontController() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        response.setContentType("text/html;charset=UTF-8");
        if(request.getSession().getAttribute("language") == null){
            request.getSession().setAttribute("language", "EN");
            LanguageBundle.addLanguage("EN.properties", request);
        }
        Command command = CommandFactory.getInstance().getCommand(request);
        String view = command.execute(request, response);
        request.getRequestDispatcher("/WEB-INF/views/" + view + ".jsp").forward(request,response);
    }
}
