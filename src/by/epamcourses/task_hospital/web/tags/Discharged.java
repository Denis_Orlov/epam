package by.epamcourses.task_hospital.web.tags;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.List;

public class Discharged extends TagSupport {
    private String listName;

    public void setListName(String value) {
        this.listName = value;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            HttpServletRequest request =  (HttpServletRequest)pageContext.getRequest();
            List<String> discharged = (List<String>)request.getSession().getAttribute(listName);
            for(String temp:discharged)
                pageContext.getOut().println(temp+"<br>");
        } catch(IOException ioException) {
            throw new JspException("Error: " + ioException.getMessage());
        }
        return SKIP_BODY;
    }

}
